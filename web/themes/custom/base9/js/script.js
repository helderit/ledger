(function ($) {
  'use strict';
  Drupal.behaviors.labelsAsPlaceholders = {
    attach: function (context, settings) {
      $('label').each(function () {
        var input = $(this).siblings('input');
        input.attr('placeholder', $(this).html());
      });
    }
  };

  Drupal.behaviors.headerFixer = {
    attach: function (context, settings) {
      $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
          $('body').addClass('scrolled');
        }
        else {
          $('body').removeClass('scrolled');
        }
      });
    }
  };

  Drupal.behaviors.jsActivation = {
    attach: function (context, settings) {
      $('body').once().addClass('js-enabled');
    }
  }

  Drupal.behaviors.searchBar = {
    attach: function (context, settings) {
      $('.search-toggler').click(function (context, settings) {
        if ($('body').hasClass('search-open')) {
          $('body').removeClass('search-open');
        }
        else {
          $('body').addClass('search-open');
        }
        return false;
      });
    }
  };

  Drupal.behaviors.mobile_menu = {
    attach: function (context, settings) {
      $('.mobile-menu-target-wrapper', context).append($('.menu--main > ul.menu').clone());

      $('.menu-toggler', context).click(function () {
        if ($('body').hasClass('menu-open')) {
          $('body').removeClass('-menu-open');
        }
        else {
          $('body').addClass('menu-open');
        }
      });

      $('.closer').click(function () {
        $('body').removeClass('menu-open');
        return false;
      });

      $('.menu-blinder').click(function () {
        $('body').removeClass('menu-open');
        return false;
      });
    }
  };
})(jQuery);