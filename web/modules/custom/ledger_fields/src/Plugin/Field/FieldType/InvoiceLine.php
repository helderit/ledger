<?php

namespace Drupal\ledger_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "invoice_line",
 *   label = @Translation("Invoice Line"),
 *   description = @Translation("Stores an invoice line."),
 *   category = @Translation("Ledger"),
 *   default_widget = "invoice_line_widget",
 *   default_formatter = "invoice_line_formatter"
 * )
 */
class InvoiceLine extends FieldItemBase {

  /**
   * Field type properties definition.
   * 
   * Inside this method we defines all the fields (properties) that our 
   * custom field type will have.
   * 
   * Here there is a list of allowed property types: https://goo.gl/sIBBgO
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties['invoice_date'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Date'));

    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Description'));

    $properties['quantity'] = DataDefinition::create('float')
      ->setLabel(t('Quantity'));

    $properties['price'] = DataDefinition::create('float')
      ->setLabel(t('Price'));

    return $properties;
  }

  public static function schema(StorageDefinition $storage) {

    $columns = [];
    $columns['invoice_date'] = [
      'type' => 'char',
      'length' => 255
    ];

    $columns['description'] = [
      'type' => 'char',
      'length' => 255
    ];
    $columns['quantity'] = [
      'type' => 'float'
    ];
    $columns['price'] = [
      'type' => 'float'
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty. 
   * 
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field fype must be considered empty.
   */
  public function isEmpty() {

    $isEmpty = 
      empty($this->get('description')->getValue()) &&
      empty($this->get('quantity')->getValue()) &&
      empty($this->get('price')->getValue());

    return $isEmpty;
  }

} // class