<?php

namespace Drupal\ledger_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "payment",
 *   label = @Translation("Payment"),
 *   description = @Translation("Stores a payment."),
 *   category = @Translation("Ledger"),
 *   default_widget = "payment_widget",
 *   default_formatter = "payment_formatter"
 * )
 */
class Payment extends FieldItemBase {
  public static function propertyDefinitions(StorageDefinition $storage) {
    $properties = [];

    $properties['payment_date'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Date'));

    $properties['paid'] = DataDefinition::create('float')
      ->setLabel(t('Amount'));

    return $properties;
  }

  public static function schema(StorageDefinition $storage) {
    $columns = [];
    $columns['payment_date'] = [
      'type' => 'char',
      'length' => 255
    ];

    $columns['paid'] = [
      'type' => 'float'
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  public function isEmpty() {
    $isEmpty = 
      empty($this->get('payment_date')->getValue()) &&
      empty($this->get('paid')->getValue());

    return $isEmpty;
  }
}
