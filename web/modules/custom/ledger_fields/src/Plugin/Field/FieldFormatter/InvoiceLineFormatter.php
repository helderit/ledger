<?php

namespace Drupal\ledger_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;
use Drupal\ledger\Util;

/**
 * Plugin implementation of the 'InvoiceLineFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "invoice_line_formatter",
 *   label = @Translation("Invoice Line"),
 *   field_types = {
 *     "invoice_line"
 *   }
 * )
 */
class InvoiceLineFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $headers = [
      "Omschrijving", 
      array('data' => "Aantal", 'class' => "text-right"), 
      array('data' => "Prijs", 'class' => "text-right"),  
      array('data' => "Totaal", 'class' => "text-right"), 
    ];
    $rows = [];

    $totalAmount = 0;

    foreach ($items as $delta => $item) {
      $quantity = $item->quantity;
      if($quantity == 0) {
        $quantity = '';
      }
      
      $rows[$delta] = [
        $item->description,
        array('data' => $quantity, 'class' => "text-right"),
        array('data' => Util::moneyFormat($item->price), 'class' => "text-right"),
        array('data' => Util::moneyFormat($item->quantity * $item->price), 'class' => "text-right")
      ];

      $totalAmount += ($item->quantity * $item->price);
    }

    $rows[] = [
      array('data' => "Totaal", 'class' => "text-strong"),
      '',
      '',
      array('data' => Util::moneyFormat($totalAmount), 'class' => "text-strong text-right")
    ];

    $rows[] = [
      "BTW 21%",
      '',
      '',
      array('data' => Util::moneyFormat($totalAmount * 0.21), 'class' => "text-right")
    ];

    $rows[] = [
      array('data' => "Totaal inclusief BTW", 'class' => "text-strong"),
      '',
      '',
      array('data' => Util::moneyFormat($totalAmount * 1.21), 'class' => 'text-strong text-right')
    ];

    $content = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows
    ];

    $elements[] = $content;
    return $elements;
  } 
}