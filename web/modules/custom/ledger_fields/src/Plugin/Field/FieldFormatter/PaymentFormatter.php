<?php

namespace Drupal\ledger_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;
use Drupal\ledger\Util;

/**
 * Plugin implementation of the 'PaymentFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "payment_formatter",
 *   label = @Translation("Payment"),
 *   field_types = {
 *     "payment"
 *   }
 * )
 */
class PaymentFormatter extends FormatterBase {
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $headers = [
      "Datum",  
      array('data' => "Betaling", 'class' => "text-right"), 
    ];
    $rows = [];

    $totalAmount = 0;

    foreach ($items as $delta => $item) {
      $rows[$delta] = [
        $item->payment_date,
        array('data' => Util::moneyFormat($item->paid), 'class' => "text-right")
      ];

      $totalAmount += $item->paid;
    }

    $rows[] = [
      array('data' => "Totaal betaald", 'class' => "text-strong"),
      array('data' => Util::moneyFormat($totalAmount, TRUE), 'class' => "text-strong text-right")
    ];

    $content = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows
    ];
    $elements[] = $content;
    return $elements;
  }
}