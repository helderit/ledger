<?php

namespace Drupal\ledger_fields\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'PaymentWidget' widget.
 *
 * @FieldWidget(
 *   id = "payment_widget",
 *   label = @Translation("Payment"),
 *   field_types = {
 *     "payment"
 *   }
 * )
 */
class PaymentWidget extends WidgetBase {
  public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  ) {

    $element['payment_date'] = [
      '#type' => 'date',
      '#title' => t('Payment Date'),
      '#date_date_format' => 'dutch_default',
      '#default_value' => isset($items[$delta]->payment_date) ? 
          $items[$delta]->payment_date : null,
      '#empty_value' => '',
    ];

    $element['paid'] = [
      '#type' => 'textfield',
      '#title' => t('Amount paid'),
      '#default_value' => isset($items[$delta]->paid) ? 
          $items[$delta]->paid : '',
      '#empty_value' => '',
      '#placeholder' => t('Amount paid'),
    ];

    return $element;
  }
}