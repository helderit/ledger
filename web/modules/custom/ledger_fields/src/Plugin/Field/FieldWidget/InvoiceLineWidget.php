<?php

namespace Drupal\ledger_fields\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'InvoiceLineWidget' widget.
 *
 * @FieldWidget(
 *   id = "invoice_line_widget",
 *   label = @Translation("Invoice Line"),
 *   field_types = {
 *     "invoice_line"
 *   }
 * )
 */
class InvoiceLineWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   * 
   * Inside this method we can define the form used to edit the field type.
   * 
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  ) {

    $element['invoice_date'] = [
      '#type' => 'date',
      '#title' => t('Invoice Date'),
      '#date_date_format' => 'dutch_default',
      '#default_value' => isset($items[$delta]->invoice_date) ? 
          $items[$delta]->invoice_date : null,
      '#empty_value' => '',
    ];

    $element['description'] = [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($items[$delta]->description) ? 
          $items[$delta]->description : null,
      '#empty_value' => '',
      '#placeholder' => t('Description'),
    ];

    $element['quantity'] = [
      '#type' => 'textfield',
      '#title' => t('Quantity'),
      '#default_value' => isset($items[$delta]->quantity) ? 
          $items[$delta]->quantity : null,
      '#empty_value' => '',
      '#placeholder' => t('Quantity'),
    ];

    $element['price'] = [
      '#type' => 'textfield',
      '#title' => t('Price'),
      '#default_value' => isset($items[$delta]->price) ? 
          $items[$delta]->price : null,
      '#empty_value' => '',
      '#placeholder' => t('Price'),
    ];

    return $element;
  }

} // class