<?php

namespace Drupal\ledger_timecards\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Link;
use Drupal\Core\Url;

class TimeCardsController extends ControllerBase {
  private $customers = [];
  private $timeCards = [];
  private $days = [];
  private $months = [];
  private $weeks = [];

  public function viewDaysDetail() {
    $this->processTimeCards();  
    $this->sortTimeCardsPerDay();

    $content = [];
    foreach($this->days as $key => $day) {
      $content[] = array(
        '#type'=>'markup',
        '#markup'=>'<h2>'.$key.'</h2>'
      );
      $content[] = $this->formatDayDetails($day);
    }

    return $this->addWrapper($content);
  }

  public function viewWeekDetail() {
    $this->processTimeCards();
    $this->sortTimeCardsPerWeek();

    $content = [];

    foreach($this->weeks as $key => $week) {
      $content[] = array(
        '#type'=>'markup',
        '#markup'=>'<h2>'.$key.'</h2>'
      );
      $content[] = $this->formatWeekDetails($week);
    }
    
    return $this->addWrapper($content);
  }

  public function viewDetailPage() {
    $this->processTimeCards();
    $this->sortTimeCardsPerMonth();

    $content = [];

    foreach($this->months as $key => $month) {
      $content[] = array(
        '#type'=>'markup',
        '#markup'=>'<h2>'.$key.'</h2>'
      );
      $content[] = $this->formatMonth($month);
    }

    return $this->addWrapper($content);
  }

  public function addWrapper($content) {
    $wrapper['wrapper'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => 'ledger-table timecards'
      ),
      '#children' => $content
    );

    return $wrapper;
  }

  public function formatWeekDetails($week) {
    $headers = array(
      "Datum",
      "Klant",
      "Omschrijving",      
      array('data' => "Uren", 'class'=>'text-right')
    );

    $totalHours = 0;
    $rows = [];

    foreach($week as $item) {
      $rows[] = array( 
        $item['date'],
        $this->customers[$item['cid']],
        $item['description'],
        array('data' => $item['hours'], 'class'=>'text-right')
      );

      $totalHours += $item['hours'];
    }

    $rows[] = array(
      array('data' => "Totaal", 'class' => 'text-strong'),
      '',
      '',
      array('data' => $totalHours, 'class' => 'text-strong text-right')  
    );

    return array(
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows
    );
  }

  public function formatMonth($month) {
    $headers = array(
      "Klant", 
      array('data' => "Uren", 'class'=>'text-right')
    );

    $totalHours = 0;
    $rows = [];

    foreach($month as $key => $customerHours) {
      $totalHours += $customerHours;
      $rows[] = array(
        $this->customers[$key],
        array('data' => $customerHours, 'class'=>'text-right')
      );
    }

    $rows[] = array(
      array('data' => "Totaal", 'class' => 'text-strong'),
      array('data' => $totalHours, 'class' => 'text-strong text-right')  
    );

    return array(
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows
    );
  }

  public function formatDayDetails($cards) {
    $headers = array(
      "Omschrijving", 
      array('data' => "Klant", 'class' => 'column-medium'), 
      array('data' => "Uren", 'class' => 'column-small')
    );
    $rows = [];

    $totalHours = 0;
    foreach($cards as $card) {
      $rows[] = array(
        $card['description'],
        $this->customers[$card['cid']],
        array('data' => $card['hours'], 'class'=>'text-right')
      );
      $totalHours += $card['hours'];
    }

    $rows[] = array(
      array('data' => "Totaal", 'class'=>'text-strong'),
      '',
      array('data' => $totalHours, 'class'=>'text-strong text-right')  
    );

    return array(
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows
    );
  }

  public function processTimeCards() {
    // Populate the customers to lessen the load on the timecard query.
    $this->populateCustomers(); 
    $this->getTimeCards();
  }

  public function sortTimeCardsPerDay() {
    foreach($this->timeCards as $timeCard) {
      $this->days[$timeCard['date']][] = $timeCard;
    }
  }

  public function sortTimeCardsPerWeek() {
    foreach($this->timeCards as $timeCard) {
      $week = $timeCard['week'];
      $this->weeks[$week][] = $timeCard;
    }
  }

  public function sortTimeCardsPerMonth() {
    foreach($this->timeCards as $timeCard) {
      $month = $timeCard['month'];
      $cid = $timeCard['cid'];

      $this->months[$month][$cid] += $timeCard['hours'];
    }
  }

  public function getTimeCards() {
    $db = \Drupal::database();
    $query = $db->select('node', 'n');
    $query->addField('nfdesc', 'field_description_value', 'description');
    $query->addField('nfdate', 'field_date_value', 'date');
    $query->addField('nfhours', 'field_hours_value', 'hours');
    $query->addField('nfcust', 'field_customer_target_id', 'cid');

    $query->join('node_field_data', 'nfdata', 'n.nid = nfdata.nid');
    $query->join('node__field_description', 'nfdesc', 'n.nid = nfdesc.entity_id');
    $query->join('node__field_date', 'nfdate', 'n.nid = nfdate.entity_id');
    $query->join('node__field_hours', 'nfhours', 'n.nid = nfhours.entity_id');
    $query->join('node__field_customer', 'nfcust', 'n.nid = nfcust.entity_id');

    $query->condition('n.type', 'timecard');
    $query->condition('nfdata.status', '1');
    $query->orderBy('date', 'DESC');

    $results = $query->execute()->fetchAll();

    foreach($results as $row) {
      $date = new DrupalDateTime($row->date);

      $this->timeCards[] = array(
        'description' => $row->description,
        'hours' => $row->hours,
        'cid' => $row->cid,
        'date' => $date->format('d-m-Y'),
        'month' => ucfirst($date->format('F Y')),
        'week' => "Week ".$date->format('W')
      );
    }
  }

  public function populateCustomers() {
    $db = \Drupal::database();
    $query = $db->select('node', 'n');
    $query->addField('nfdata', 'title');
    $query->addField('n', 'nid');
    $query->join('node_field_data', 'nfdata', 'n.nid = nfdata.nid');
    $query->condition('n.type', 'customer');

    $results = $query->execute()->fetchAll();
    
    foreach($results as $key => $row) {
      $url = Url::fromRoute('entity.node.canonical', ['node' => $row->nid]);
      $link = Link::fromTextAndUrl($row->title, $url);
      $this->customers[$row->nid] = $link;
    }
  }
}