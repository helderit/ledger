<?php

namespace Drupal\ledger;

class Util {
  public static function moneyFormat($amount, $force = FALSE) {
    if($amount == 0 && !$force) {
      return '';
    }
    return '€ ' . number_format($amount, 2, ',', '.');
  }
}