<?php

namespace Drupal\ledger_views_fields\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\node\Entity\Node;

use Drupal\ledger\Util;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("total_vat")
 */
class TotalVAT extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return $this->getTotalAmountVAT($values->nid);
  }

  public function getTotalAmountVAT($nid) {
    $total = 0;

    $node = Node::load($nid);
    $invoiceLines = $node->get('field_invoice_lines')->getValue();

    foreach($invoiceLines as $invoiceLine) {
      $total += ($invoiceLine["quantity"] * $invoiceLine["price"] * 1.21);
    }
    
    return Util::moneyFormat($total);
  }

}
