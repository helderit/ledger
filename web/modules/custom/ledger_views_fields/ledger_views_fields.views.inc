<?php

/**
 * @file
 * Contains ledger_views_fields\ledger_views_fields.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */


/**
* Implements hook_views_data().
*/
function ledger_views_fields_views_data() {

    $data['views']['table']['group'] = t('Ledger');
    $data['views']['table']['join'] = [
      // #global is a special flag which allows a table to appear all the time.
      '#global' => [],
    ];

    $data['views']['total_amount'] = [
        'title' => t('Total amount'),
        'help' => t('Total amount of a invoiceline'),
        'field' => [
            'id' => 'total_amount',
        ],
    ];

    $data['views']['total_paid'] = [
        'title' => t('Total paid'),
        'help' => t('Total paid'),
        'field' => [
            'id' => 'total_paid',
        ],
    ];

    $data['views']['total_vat'] = [
        'title' => t('Totaal in BTW'),
        'help' => t('Total VAT'),
        'field' => [
            'id' => 'total_vat',
        ],
    ];
    return $data;
}
