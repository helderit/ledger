<?php

namespace Drupal\ledger_rides_administration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Link;
use Drupal\Core\Url;

class RidesController extends ControllerBase {
  private $rides = [];
  private $months = [];



  public function viewPage() {
    $this->fetchRides();
    $this->sortRidesMonth();

    $content = [];
    
    $content[] = array(
        '#type'=>'markup',
        '#markup'=>'<a href="/node/add/ride_administration?destination=/rides" class="button button-add">Rit toevoegen</a>'
      );

    foreach($this->months as $key => $month) {
      $content[] = array(
        '#type'=>'markup',
        '#markup'=>'<h2>'.$key.'</h2>'
      );

      $content[] = $this->formatRides($month);
    }
    
    return $this->addWrapper($content);
  }

  public function addWrapper($content) {
    $wrapper['wrapper'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => 'ledger-table rides'
      ),
      '#children' => $content
    );

    return $wrapper;
  }

  public function sortRidesMonth() {
    foreach($this->rides as $ride) {
      $this->months[$ride['month']][] = $ride;
    }
  }

  public function formatRides($rides) {
    $headers = array(
      "Datum",
      "Van",
      "Naar",
      "Reden",   
      array('data' => "Kilometers", 'class'=>'text-right')
    );

    $totalKM = 0;

    foreach($rides as $item) {
      if($item['isRoundTrip']) {
        $rows[] = array( 
          $item['date'],
          $item['toCompany'] . ' ('.$item['toPC'].')',
          $item['fromCompany'] . ' ('.$item['fromPC'].')',
          $item['description'],
          array('data' => $item['kilometers'], 'class'=>'text-right')
        );
        $totalKM += $item['kilometers'];
      }
      
      $rows[] = array( 
        $item['date'],
        $item['fromCompany'] . ' ('.$item['fromPC'].')',
        $item['toCompany'] . ' ('.$item['toPC'].')',
        $item['description'],
        array('data' => $item['kilometers'], 'class'=>'text-right')
      );

      $totalKM += $item['kilometers'];
    }

    $rows[] = array(
      array('data' => "Totaal", 'class' => 'text-strong'),
      '',
      '',
      '',
      array('data' => $totalKM, 'class' => 'text-strong text-right')  
    );

    return array(
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows
    );
  }

  public function fetchRides() {
    $db = \Drupal::database();
    $query = $db->select('node', 'n');
    
    $query->addField('nfk', 'field_kilometers_value', 'kilometers');
    $query->addField('nfd', 'field_date_value', 'date');
    $query->addField('nfr', 'field_roundtrip_value', 'roundtrip');

    $query->addField('nfpcf', 'field_postal_code_from_postal_code', 'fromPC');
    $query->addField('nfpcf', 'field_postal_code_from_organization', 'fromCompany');

    $query->addField('nfpct', 'field_postal_code_to_postal_code', 'toPC');
    $query->addField('nfpct', 'field_postal_code_to_organization', 'toCompany');

    $query->addField('nfdesc', 'field_description_value', 'description');

    $query->join('node_field_data', 'nfdata', 'n.nid = nfdata.nid');
    $query->join('node__field_kilometers', 'nfk', 'n.nid = nfk.entity_id');
    $query->join('node__field_date', 'nfd', 'n.nid = nfd.entity_id');
    $query->leftJoin('node__field_description', 'nfdesc', 'n.nid = nfdesc.entity_id');
    $query->join('node__field_roundtrip', 'nfr', 'n.nid = nfr.entity_id');
    $query->join('node__field_postal_code_from', 'nfpcf', 'n.nid = nfpcf.entity_id');
    $query->join('node__field_postal_code_to', 'nfpct', 'n.nid = nfpct.entity_id');
    $query->condition('n.type', 'ride_administration');
    $query->condition('nfdata.status', '1');
    $query->orderBy('date', 'DESC');

    $results = $query->execute()->fetchAll();

    foreach($results as $row) {
      $date = new DrupalDateTime($row->date);
      $this->rides[] = array(
        'date' => $date->format('d-m-Y'),
        'description' => $row->description,
        'kilometers' => $row->kilometers,
        'month' => ucfirst($date->format('F Y')),
        'isRoundTrip' => $row->roundtrip,
        'fromPC' => $row->fromPC,
        'toPC' => $row->toPC,
        'fromCompany' => $row->fromCompany,
        'toCompany' => $row->toCompany
      );
    }
  }
}